FROM bitnami/minideb:stretch
ADD .  /usr/bin 
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update  && apt-get upgrade -y && apt-get install -y --no-install-recommends apt-utils && apt-get install nano &&  apt-get install -y gnupg
RUN apt-get install -y -q --no-install-recommends \
        apt-transport-https \
        build-essential \
        ca-certificates \
        curl \
        git \
        libssl-dev \
        python3-minimal \
        rsync \
        software-properties-common \
        devscripts \
        autoconf \
        ssl-cert \
    && apt-get clean
RUN apt-get install wget -y 
# install google chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list' &&  apt-get -y update &&  apt-get install -y google-chrome-stable &&  apt install python3-pip  --upgrade  -y &&  pip3 install selenium beautifulsoup4
WORKDIR /home/pythonApp
