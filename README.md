# Docker compose: debian mini with python chromedriver bs4 and mongodb

It is a Docker-compose and Dockerfile to set an environment for working with selenium and chromedriver for web scrapping purposes  and save this information in a mongo database. 
The web browser is obviously Chrome.
This configuration comes with two bind mount volumes one for the mongo data and one for the python app where you could store your app.